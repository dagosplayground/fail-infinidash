package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

type HeaderTemplate struct {
	HomeActive    bool
	DashActive    bool
	AppActive     bool
	LoginActive   bool
	BodyParagraph string
}

func htHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/ht" && r.URL.Path != "/" {
		fmt.Printf("Path not found: %s\n", r.URL.Path)
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		fmt.Printf("Method in ht: %s\n", r.Method)
		http.Error(w, "method is not supported", http.StatusNotFound)
		return
	}
	tmpl, tmplErr := template.ParseFiles("templates/ht.html")
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}

	data := HeaderTemplate{
		HomeActive:    true,
		DashActive:    false,
		AppActive:     false,
		LoginActive:   false,
		BodyParagraph: "Home string.",
	}

	errEx := tmpl.Execute(w, data)
	if errEx != nil {
		log.Println(errEx)
	}
}

func main() {
	PORT := ":8001"
	arguments := os.Args
	if len(arguments) == 2 {
		PORT = ":" + arguments[1]
	}
	fmt.Printf("Using default port number: %s\n", PORT)

    fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)
	http.HandleFunc("/ht", htHandler)
	http.HandleFunc("/dashboard", dashboardHandler)
	http.HandleFunc("/app", appHandler)
	http.HandleFunc("/login", loginHandler)

	if err := http.ListenAndServe(PORT, nil); err != nil{
		log.Fatal(err)
	}
}
